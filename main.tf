#1 -this will create a S3 bucket in AWS
resource "aws_s3_bucket" "terraform_state_s3-cobrakai" {
  #make sure you give unique bucket name
  bucket = "terraform-cobrakai-state"
  lifecycle {
    prevent_destroy = false
  }
}

resource "aws_s3_bucket_versioning" "versioning_example" {
  bucket = aws_s3_bucket.terraform_state_s3-cobrakai.id
  versioning_configuration {
    status = "Enabled"
  }
}

resource "aws_kms_key" "mykey" {
  description             = "This key is used to encrypt bucket objects"
  deletion_window_in_days = 10
}


resource "aws_s3_bucket_server_side_encryption_configuration" "example" {
  bucket = aws_s3_bucket.terraform_state_s3-cobrakai.bucket

  rule {
    apply_server_side_encryption_by_default {
      kms_master_key_id = aws_kms_key.mykey.arn
      sse_algorithm     = "aws:kms"
    }
  }
}

# 2 - this Creates Dynamo Table
resource "aws_dynamodb_table" "terraform_locks" {
  # Give unique name for dynamo table name
  name         = "tf-up-and-run-locks-cobrakai"
  billing_mode = "PAY_PER_REQUEST"
  hash_key     = "LockID"
  attribute {
    name = "LockID"
    type = "S"
  }
}

terraform {
  backend "s3" {
    #Replace this with your bucket name!
    bucket = "terraform-cobrakai-state"
    key    = "terraform.tfstate"
    region = "us-east-1"
    #Replace this with your DynamoDB table name!
    dynamodb_table = "tf-up-and-run-locks-cobrakai"
    encrypt        = true

  }
}

#####
# Modules sections
####
module "security_group" {
  source      = "./modules/sg"
  name        = "cobrakai-sg"
  description = "My security group for cobrakai"
  vpc_id      = module.vpc.vpc_id

  ingress_cidr_blocks = ["0.0.0.0/0"]
  ingress_rules       = ["ssh-22-tcp", "http-80-tcp", "https-443-tcp"]

  egress_cidr_blocks = ["0.0.0.0/0"]
  egress_rules       = ["any"]

  tags = {
    Terraform   = "true"
    Environment = "dev"
  }
}

module "ec2" {
  source        = "./modules/ec2"
  ec2_count     = var.ec2_count
  instance_type = var.instance_type
  subnet_id     = module.vpc.subnet_id ## we bring the subnet from the module vpc output file
  vpc_id        = module.vpc.vpc_id
  #sg_id         = module.security_group.id
}

module "ec2-prod-instance" {
  source        = "./modules/ec2"
  ec2_count     = var.ec2_count
  instance_type = var.instance_type
  subnet_id     = module.vpc.subnet_id2 ## we bring the subnet from the module vpc output file
  #sg_id         = module.security_group.id
  type_instance = "serverprod"
  vpc_id        = module.vpc.vpc_id
}

#external module from aws vpc
module "vpc" {
  source              = "./modules/vpc"
  region_provider     = var.region_provider
  vpc_cidr_block      = var.vpc_cidr_block
  map_private_subnets = var.map_private_subnets
  env_prefix          = var.env_prefix
  map_public_subnets  = var.map_public_subnets
  basename            = var.basename

}

#eks mnodule
module "eks" {
  source      = "./modules/eks"
  subnet_id_1 = module.vpc.priv_subnet_id
  subnet_id_2 = module.vpc.priv_subnet_id2

}







/*

## EKS module deploy
locals {
  cluster_name = "my-eks-cluster"
}
module "eks" {
  source          = "terraform-aws-modules/eks/aws"
  version         = "17.24"
  cluster_name    = local.cluster_name
  cluster_version = "1.20"
  subnets         = [module.vpc.priv_subnet_id, module.vpc.priv_subnet_id2]

  vpc_id = module.vpc.vpc_id

  workers_group_defaults = {
    root_volume_type = "gp2"
  }

  worker_groups = [
    {
      name                          = "worker-group-1"
      instance_type                 = "t2.small"
      additional_userdata           = "echo foo bar"
      additional_security_group_ids = [aws_security_group.worker_group_mgmt_one.id]
      asg_desired_capacity          = 2
    },
    {
      name                          = "worker-group-2"
      instance_type                 = "t2.medium"
      additional_userdata           = "echo foo bar"
      additional_security_group_ids = [aws_security_group.worker_group_mgmt_two.id]
      asg_desired_capacity          = 1
    },
  ]
}

module "eks_auth" {
  source = "aidanmelen/eks-auth/aws"
  eks    = module.eks

  map_roles = [
    {
      rolearn  = "arn:aws:iam::543467197432:role/aws-service-role/eks.amazonaws.com/AWSServiceRoleForAmazonEKS"
      username = "role-eks-admin"
      groups   = ["system:masters"]
    },
  ]

  map_users = [
    {
      userarn  = "arn:aws:iam::543467197432:user/nacho"
      username = "nacho"
      groups   = ["system:masters"]
    },
    {
      userarn  = "arn:aws:iam::543467197432:user/admin_gabi"
      username = "admin_gabi"
      groups   = ["system:masters"]
    },
{
      userarn  = "arn:aws:iam::543467197432:user/terraform"
      username = "terraform"
      groups   = ["system:masters"]
    },
    
  ]

  map_accounts = [
    "nacho",
    "admin_gabi",
    "terraform"
  ]
}

data "aws_eks_cluster" "cluster" {
  name = module.eks.cluster_id
}

data "aws_eks_cluster_auth" "cluster" {
  name = module.eks.cluster_id
}

*/


