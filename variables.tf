variable "region_provider" {}
variable "vpc_cidr_block" {}
variable "env_prefix" {}
#variable "my_ip" {}
variable "instance_type" {}
#variable "public_key_location" {}
#variable "image_name" {}
variable "map_private_subnets" {
  type = map(object({
    az   = string
    cidr = string
  }))

}
variable "map_public_subnets" {
  type = map(object({
    az   = string
    cidr = string
  }))
}
variable "basename" {}


## variables for ec2 module
variable "ec2_count" {}



## variables for security group
variable "rules" {
  type = map(any)
  default = {
    any            = [0, 0, "-1", "ANY"]
    icmp           = ["-1", "-1", "icmp", "ICMP"]
    ssh-22-tcp     = [22, 22, "tcp", "HTTP"]
    http-80-tcp    = [80, 80, "tcp", "HTTP"]
    https-443-tcp  = [443, 443, "tcp", "HTTPS"]
    ldap-389-tcp   = [389, 389, "tcp", "HTTP"]
    mysql-3306-tcp = [3306, 3306, "tcp", "MYSQL"]
  }
}



#variable "subnet_cidr_block" {
# type = list(string)
#}
#variable "azs_list" {
#   type = list(string)
#}

#variable "public_key_location" {
# default = "/Users/jpiazza/.ssh/cobrakai2022.pem.pub"
#"amzn2-ami-hvm-*-x86_64-gp2"
#}
