

module "vpc" {
  source              = "./modules/vpc"
  region_provider     = var.region_provider
  vpc_cidr_block      = var.vpc_cidr_block
  map_private_subnets = var.map_private_subnets
  env_prefix          = var.env_prefix
  map_public_subnets  = var.map_public_subnets
  basename            = var.basenamee

}


module "ec2" {
  source        = "./modules/ec2"
  ec2_count     = var.ec2_count
  instance_type = var.instance_type
  subnet_id     = module.vpc.subnet_id ## we bring the subnet from the module vpc output file
  vpc_id        = module.vpc.vpc_id
  #sg_id         = module.security_group.id
}
