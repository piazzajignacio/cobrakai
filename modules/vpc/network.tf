
resource "aws_vpc" "cobrakaivpc" {
    cidr_block = "${var.vpc_cidr_block}"
    instance_tenancy = "default"
    enable_dns_hostnames = "true"


    tags = {
      "Name" = "${var.env_prefix}-master-vpc"
      Location = "US"

    }  
}

## we create public subnets using map
resource "aws_subnet" "public-subnets" {
  for_each = var.map_public_subnets
 
  availability_zone_id = each.value["az"]
  cidr_block = each.value["cidr"]
  vpc_id     = "${aws_vpc.cobrakaivpc.id}"

  tags = {
    Name = "${var.basename}-subnet-${each.key}"
  }
}

## we create private subnets using map
resource "aws_subnet" "private-subnets" {
  for_each = var.map_private_subnets
 
  availability_zone_id = each.value["az"]
  cidr_block = each.value["cidr"]
  vpc_id     = "${aws_vpc.cobrakaivpc.id}"
  map_public_ip_on_launch = true
  tags = {
    Name = "${var.basename}-subnet-${each.key}"
  }
}


/*
##crear un for_each en la creacion de subnet (por az)
## we get all AZ
resource "aws_subnet" "subnets" {
    #FIRST OPTION
    #count = "${length(var.azs_list)}"
    #vailability_zone = "${element(var.azs_list, count.index)}"
    #SECOND OPTION
    count = "${length(data.aws_availability_zones.azs.names)}"
    availability_zone = "${element(data.aws_availability_zones.azs.names, 0)}"
    vpc_id = "${aws_vpc.cobrakaivpc.id}"
    cidr_block = "${element(var.subnet_cidr_block, count.index)}"
    
    tags = {
      "Name" = "Subnet-${count.index +1}"
    }
}

*/
resource "aws_internet_gateway" "gw" {
  vpc_id = "${aws_vpc.cobrakaivpc.id}"

  tags = {
    Name = "${var.basename}-igw"
  }

}
resource "aws_route_table" "cobrakai-rt" {
  vpc_id = "${aws_vpc.cobrakaivpc.id}"
  
  route {
      cidr_block = "0.0.0.0/0"
      gateway_id = "${aws_internet_gateway.gw.id}"
    }
  
  tags = {
    Name = "${var.basename}-rtb"
  }
}


resource "aws_route_table_association" "public-subnet-rta" {
  for_each = var.map_public_subnets
  subnet_id = "${aws_subnet.public-subnets[each.key].id}"
  #subnet_id = "${element(aws_subnet.public-subnets.*.id)}"
  route_table_id = "${aws_route_table.cobrakai-rt.id}"
}


resource "aws_route_table_association" "private-subnet-rta" {
  for_each = var.map_private_subnets
  subnet_id = "${aws_subnet.private-subnets[each.key].id}"
  #subnet_id = "${element(aws_subnet.public-subnets.*.id)}"
  route_table_id = "${aws_route_table.cobrakai-rt.id}"
}