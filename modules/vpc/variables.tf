variable "region_provider" {}
variable "vpc_cidr_block" {}
variable "env_prefix" {}

variable "basename" {
   description = "Prefix used for all resources names"
   default = "cobrakai"
}

variable "map_private_subnets" {
   type = map
   default = {
     private-sub-1 = {
         az = "use1-az2"
         cidr = "10.0.10.0/24"
      }
      private-sub-2 = {
         az = "use1-az4"
         cidr = "10.0.20.0/24"
      }
      private-sub-3 = {
         az = "use1-az6"
         cidr = "10.0.30.0/24"
      }

  }
}

variable "map_public_subnets" {
   type = map
   default = {
      public-sub-1 = {
         az = "use1-az2"
         cidr = "10.0.40.0/24"
      }
      public-sub-2 = {
         az = "use1-az4"
         cidr = "10.0.50.0/24"
      }
      public-sub-3 = {
         az = "use1-az6"
         cidr = "10.0.60.0/24"
      }
   }
}

