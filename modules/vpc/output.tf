output "vpc_id" {
  value = "${aws_vpc.cobrakaivpc.id}"
}



output "public_subnet_ids" {
 value = "${aws_subnet.public-subnets}"
}
output "priv_subnet_ids" {
 value = "${aws_subnet.private-subnets}"
}

output "subnet_id" {
 value = "${aws_subnet.public-subnets["public-sub-1"].id}"
}

output "subnet_id2" {
 value = "${aws_subnet.public-subnets["public-sub-2"].id}"
}
output "subnet_id3" {
 value = "${aws_subnet.public-subnets["public-sub-3"].id}"
}


output "priv_subnet_id" {
 value = "${aws_subnet.private-subnets["private-sub-1"].id}"
}
output "priv_subnet_id2" {
 value = "${aws_subnet.private-subnets["private-sub-2"].id}"
}
output "priv_subnet_id3" {
 value = "${aws_subnet.private-subnets["private-sub-3"].id}"
}


/*
output "sg_id" {
  value = "${aws_security_group.server.id}"
}
*/