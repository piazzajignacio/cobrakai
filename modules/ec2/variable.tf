variable "ec2_count" {
  default = "1"
}


variable "instance_type" {
  default = "t2.micro"
}

variable "subnet_id" {} ##lo dejamos asi por que lo vamos a usar con los outputs.

#variable "sg_id" {}  



variable "type_instance" {
  type = string
  default = "servertest"
  
}
variable "map_of_type_instances" {
    type = map(string)

    default = {
      servertest = "server-test"
      serverprod = "server-prod"
      server-jump = "server-jump"
}
}
variable "vpc_id" {}
