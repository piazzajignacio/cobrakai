data "aws_ami" "latest_aws_linux_image"{
    most_recent = true
    owners =  ["amazon"] 
    filter {
        name = "name"
        values = ["amzn2-ami-kernel-*-x86_64-gp2"]
    }
}

resource "random_pet" "instance" {
  
}
resource "aws_instance" "instace" {
  count                       = "${var.ec2_count}"
  ami                         = "${data.aws_ami.latest_aws_linux_image.id}"
  instance_type               = "${var.instance_type}"
  subnet_id                   = "${var.subnet_id}"
  #vpc_security_group_ids      = ["${var.sg_id}"] ##the security groups always are set among []
  security_groups = ["${aws_security_group.instance-sg.id}"]
  associate_public_ip_address = "true"

  tags = {
    "Name" = "${lookup(var.map_of_type_instances, var.type_instance)}"
  }
}

resource "aws_security_group" "instance-sg" {
  name = "${var.type_instance}+${random_pet.instance.id}"
  description = "security group for instance"
  vpc_id = "${var.vpc_id}"
  ingress {
    from_port = 80
    to_port = 80
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port = 22
    to_port = 22
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]

  }
  egress {
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}
